import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {ImagesContainerComponent} from './images-container.component';
import {GetImagesService, IImageData, IImagePage} from '../../services/get-images/get-images.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {ImageTileComponent} from '../image-tile/image-tile.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

describe('ImagesContainerComponent', () => {
  let component: ImagesContainerComponent;
  let fixture: ComponentFixture<ImagesContainerComponent>;

  let mockComponent: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientTestingModule, HttpClientModule, NgbModule.forRoot()],
      declarations: [ImagesContainerComponent, ImageTileComponent],
      providers: [GetImagesService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesContainerComponent);
    component = fixture.componentInstance;
    mockComponent = component;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default properties', () => {
    expect(component.noMatch).toBeFalsy();
    expect(component.pageNumber).toEqual(1);
  });

  describe('getCurrentPage', () => {
    it('should subscribe to currentPage property of GetImagesService and assign returned value to local page property',
      inject([GetImagesService], (service: GetImagesService) => {

        const mockImages: IImageData[] = [
          {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
          {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''}
        ];

        const mockPage: IImagePage = {
          images: mockImages,
          index: 1
        };

        component.ngOnInit();
        service.currentPage.next(mockPage);
        expect(component.page).toEqual(mockPage);
      }));

    it('should set local property noMatch to true if the page is empty', inject([GetImagesService], (service: GetImagesService) => {

      const mockImages: IImageData[] = [];
      const mockPage: IImagePage = {
        images: mockImages,
        index: 1
      };
      component.ngOnInit();
      service.currentPage.next(mockPage);
      expect(component.noMatch).toBeTruthy();
    }));
  });

  describe('getPagesAmount', () => {
    it('should subscribe to filteredPagesAmount property of GetImagesService and assign returned value to local property',
      inject([GetImagesService], (service: GetImagesService) => {
        component.ngOnInit();
        service.filteredPagesAmount.next(10);
        expect(component.pagesAmount).toEqual(100); // multiplied by 10 to accomodate for the selected ng paginator
      }));

    it('should subscribe set local pageNumber property to 1',
      inject([GetImagesService], (service: GetImagesService) => {
        component.pageNumber = 5;
        component.ngOnInit();
        service.filteredPagesAmount.next(10);
        expect(component.pageNumber).toEqual(1);
      }));
  });
});
