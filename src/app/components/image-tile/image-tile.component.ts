import {Component, Input} from '@angular/core';
import {IImageData} from '../../services/get-images/get-images.service';

@Component({
  selector: 'app-image-tile',
  templateUrl: './image-tile.component.html',
  styleUrls: ['./image-tile.component.scss']
})

/**
 * component for a single image tile
 * @class ImageTileComponent
 */
export class ImageTileComponent {

  /**
   * passed image data
   * @property imageData
   * @public
   */
  @Input() imageData: IImageData;

  /**
   * used for lazy loading of the images
   * if image is loaded its set to true using defer load
   * @property showImage
   * @public
   */
  public showImage = false;

  constructor() {
  }
}
