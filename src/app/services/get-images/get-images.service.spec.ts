import {TestBed, inject, getTestBed, async} from '@angular/core/testing';

import {GetImagesService, IImageData} from './get-images.service';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('GetImagesService', () => {

  // used to test private properties of the service, otherwise not accessible
  let mockService: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [GetImagesService]
    });

    mockService = TestBed.get(GetImagesService) as any;
  });

  it('should be created', inject([GetImagesService], (service: GetImagesService) => {
    expect(service).toBeTruthy();
  }));

  it('should have default properties', inject([GetImagesService], (service: GetImagesService) => {
    expect(mockService._jsonUrl).toEqual('http://jsonplaceholder.typicode.com/photos');
    expect(mockService._allImages).toEqual([]);
    expect(mockService._filteredPages).toEqual([]);
  }));

  describe('getImages', () => {
    it(`should make a http request to retrieve data assign it to _allImages property`,
      async(
        inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {

          const mockData: IImageData[] = [
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''}
          ];

          const req = backend.expectOne({
            url: 'http://jsonplaceholder.typicode.com/photos',
            method: 'GET'
          });

          req.flush(mockData);

          expect(mockService._allImages).toEqual(mockData);
          mockService.getImages();
        })
      )
    );

    it(`should call splitIntoPages method after http request is returned`,
      async(
        inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {

          const spy = spyOn(mockService, 'splitIntoPages').and.callThrough();

          const mockData: IImageData[] = [
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
            {albumId: 1, id: 1, title: '', url: '', thumbnailUrl: ''},
          ];

          const req = backend.expectOne({
            url: 'http://jsonplaceholder.typicode.com/photos',
            method: 'GET'
          });

          req.flush(mockData);
          mockService.getImages();

          expect(spy).toHaveBeenCalledWith(mockData, 21);
        })
      )
    );
  });

  describe('filterImages', () => {
    it('should filter _allImages property based on search query passed into the method and generate filtered pages based on the filter result', () => {
      const mockData: IImageData[] = [
        {albumId: 1, id: 1, title: 'title1', url: '', thumbnailUrl: ''},
        {albumId: 1, id: 1, title: 'title2', url: '', thumbnailUrl: ''},
        {albumId: 1, id: 1, title: 'title3', url: '', thumbnailUrl: ''},
        {albumId: 1, id: 1, title: 'title4', url: '', thumbnailUrl: ''},
      ];

      mockService._allImages = mockData;

      const searchQuery = 'title1';
      mockService.filterImages(searchQuery);

      expect(mockService._filteredPages[0].images).toEqual([{albumId: 1, id: 1, title: 'title1', url: '', thumbnailUrl: ''}]);

    });
  });

});
