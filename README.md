# RunpathTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.1.

Halfway through the project I updated my Angular to cli to 7 (see package.json)

## Overview

The purpose of this project is to demonstrate production ready code for a single page application which displays a paginated grid of images and allows the user to filter through them using search queries. 
The view is optimised for both desktop and mobile.
 
## Architecture / Structure

The composition of this project consists of 4 Components and 1 Service - detailed below
For testing, it uses Karma + Jasmine.

### GetImagesService
- retrieves all image data from a remote url
- handles ordering and paginating of the data
- components that require this data utilise this service via subscription

### SearchBoxComponent
- displayed at the top of the app view
- used to pass search queries to GetImagesService

### ImagesContainerComponent
- displayed under the search box
- generates a grid of ImageTileComponent instances
- uses Bootstrap ngb pagination
- subscribes to GetImagesService to retrieve the current page and total pages amount
= handles opening of the modal when an image is clicked

### ImageTileComponent
- single image tile component
- displays the image along with its title   

### ImageModal
- uses Bootstrap ngb modal component 
- displays a larger image in a modal window

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
