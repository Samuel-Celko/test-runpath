import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { ImageTileComponent } from './components/image-tile/image-tile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GetImagesService} from './services/get-images/get-images.service';
import {HttpClientModule} from '@angular/common/http';
import { ImagesContainerComponent } from './components/images-container/images-container.component';
import {DeferLoadModule} from '@trademe/ng-defer-load';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ImageModalComponent } from './components/image-modal/image-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchBoxComponent,
    ImageTileComponent,
    ImagesContainerComponent,
    ImageModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DeferLoadModule,
    NgbModule.forRoot()
  ],
  entryComponents: [ImageModalComponent],
  providers: [
    GetImagesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
