import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {SearchBoxComponent} from './search-box.component';
import {ReactiveFormsModule} from '@angular/forms';
import {GetImagesService} from '../../services/get-images/get-images.service';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('SearchBoxComponent', () => {

  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientTestingModule, HttpClientModule],
      declarations: [SearchBoxComponent],
      providers: [GetImagesService]
    })
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default properties', () => {
    expect(component.searchEmpty).toBeTruthy();
  });

  it('should subscribe to form control events after ngOnInit lifecycle is called', () => {
    component.searchCtrl.valueChanges.subscribe(value => {
      expect(value).toEqual('Ars');
    });
    component.searchCtrl.setValue('Ars');
    component.ngOnInit();
  });

  it('should call filterImages method of GetImagesService when search query is entered',
    inject([GetImagesService], (service: GetImagesService) => {
    const spy = spyOn(service, 'filterImages');

    component.searchCtrl.valueChanges.subscribe(value => {
      expect(spy).toHaveBeenCalledWith('test');
    });
    component.searchCtrl.setValue('test');
  }));

  describe('reset', () => {
    it('should call reset method of searchCtrl FormController', () => {
      const spy = spyOn(component.searchCtrl, 'reset').and.callThrough();
      component.reset();
      expect(spy).toHaveBeenCalled();
    });

    it('should set searchEmpty boolean to true', () => {
      component.reset();
      expect(component.searchEmpty).toBeTruthy();
    });
  });

});
