import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {SearchBoxComponent} from './components/search-box/search-box.component';
import {ImageTileComponent} from './components/image-tile/image-tile.component';
import {ImagesContainerComponent} from './components/images-container/images-container.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {GetImagesService} from './services/get-images/get-images.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule, ReactiveFormsModule, NgbModule.forRoot()],
      providers: [GetImagesService],
      declarations: [
        AppComponent,
        SearchBoxComponent,
        ImagesContainerComponent,
        ImageTileComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
