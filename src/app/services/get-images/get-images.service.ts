import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()

/**
 * handles http request to get local json with all images
 * holds reference to all images
 * filters all images by search query and holds a reference to all filtered images
 * @class GetImagesService
 */
export class GetImagesService {

  /**
   * relative url to json file with image data
   * @property _jsonUrl
   * @type {string}
   * @private
   */
  private _jsonUrl = 'http://jsonplaceholder.typicode.com/photos';

  /**
   * holds all json data retrieved
   * @property _allImages
   * @type {IImageData[]}
   * @private
   */
  private _allImages: IImageData[] = [];

  /**
   * all pages that match current search filter
   * @property filteredPages
   * @type {IImagePage[]}
   */
  private _filteredPages: IImagePage[] = [];

  /**
   * amount of filtered pages
   * @property filteredPagesAmount
   * @public
   */
  public filteredPagesAmount: BehaviorSubject<number>;

  /**
   * current page
   * @property currentPage
   * @public
   */
  public currentPage: BehaviorSubject<IImagePage>;

  /**
   * @constructor
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.filteredPagesAmount = new BehaviorSubject<number>(0);
    this.currentPage = new BehaviorSubject<IImagePage>(null);
    this.getImages();
  }

  /**
   * makes http request to retrieve json from the endpoint
   * assigns the result to local _allImages generates pages
   * @method getImages
   */
  private getImages(): void {
    this._httpClient.get(this._jsonUrl).subscribe((images: IImageData[]) => {
      this._allImages = images;
      this._filteredPages = this.splitIntoPages(images, 21);
      this.filteredPagesAmount.next(this._filteredPages.length);
      this.currentPage.next(this._filteredPages[0]);
    }, (error: Error) => {
      console.log(error, 'error getting json data');
    });
  }

  /**
   * splits an array of image data into pages and returns a paginated array
   * @method splitIntoPages
   * @param {IImageData[]} imagesArray
   * @param {number} imagesPerPage
   * @returns {Array<IImagePage>}
   */
  private splitIntoPages(imagesArray: IImageData[], imagesPerPage: number): Array<IImagePage> {
    const paginatedArray: Array<IImagePage> = [];
    const imagesArrayCopy = [...imagesArray];
    const numOfChild = Math.ceil(imagesArrayCopy.length / imagesPerPage);
    for (let i = 0; i < numOfChild; i++) {
      paginatedArray.push(
        {
          images: imagesArrayCopy.splice(0, imagesPerPage),
          index: i
        }
      );
    }
    return paginatedArray;
  }

  /**
   * filters _allImages based on the passed in search query and updates _filteredPages with the result
   * @method filterImages
   * @param {string} searchValue
   * @public
   */
  public filterImages(searchValue: string): void {
    const filteredImages = this._allImages.filter(image => image.title.toLowerCase().indexOf(searchValue.toLowerCase()) > -1);
    this._filteredPages = this.splitIntoPages(filteredImages, 20);
    this.filteredPagesAmount.next(this._filteredPages.length);
    const _currentPage: IImagePage = this._filteredPages.length > 0 ? this._filteredPages[0] : {images: [], index: 0};
    this.currentPage.next(_currentPage);
  }

  /**
   * changes current page number
   * @method changePage
   * @param {number} pageNumber
   */
  public changePage(pageNumber: number): void {
    this.currentPage.next(this._filteredPages[pageNumber - 1]);
  }
}

/**
 * type interface for image data
 */
export interface IImageData {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}

/**
 * type interface for one page
 */
export interface IImagePage {
  images: IImageData[];
  index: number;
}

