import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {GetImagesService} from '../../services/get-images/get-images.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})

/**
 * search box component, displayed at the top of the app view
 * used to pass search queries to GetImagesService
 * @class SearchBoxComponent
 */
export class SearchBoxComponent implements OnInit, OnDestroy {

  /**
   * form control used on search input
   * @property searchCtrl
   * @type {FormControl}
   * @public
   * @default {new FormControl()}
   */
  public searchCtrl: FormControl = new FormControl();

  /**
   * true if search box is empty
   * @property searchEmpty
   * @type {boolean}
   * @default {true}
   */
  public searchEmpty = true;

  /**
   * holds reference to searchCtrl valueChanges subscription
   * @property _searchFormControlSub
   * @private
   */
  private _searchFormControlSub: Subscription;

  /**
   * @constructor
   * @param {GetImagesService} _getImagesService
   */
  constructor(private _getImagesService: GetImagesService) {
  }

  /**
   * angular on init lifecycle
   * subscribes to search input form control value
   * @public
   */
  public ngOnInit(): void {
    this._searchFormControlSub = this.searchCtrl.valueChanges.subscribe((searchValue: string) => {
      this.searchEmpty = searchValue === null || searchValue === '' ? true : false;
      searchValue = searchValue === null ? '' : searchValue;
      this._getImagesService.filterImages(searchValue);
    });
  }

  /**
   * resets search box input
   * @method reset
   * @public
   */
  public reset(): void {
    this.searchCtrl.reset();
    this.searchEmpty = true;
  }

  /**
   * angular on destroy lifecycle
   * @method ngOnDestroy
   * @public
   */
  public ngOnDestroy(): void {
    this._searchFormControlSub.unsubscribe();
  }
}
