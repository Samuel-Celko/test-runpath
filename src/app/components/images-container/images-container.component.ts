import {Component, OnDestroy, OnInit} from '@angular/core';
import {GetImagesService, IImageData, IImagePage} from '../../services/get-images/get-images.service';
import {Subscription} from 'rxjs/Subscription';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ImageModalComponent} from '../image-modal/image-modal.component';

@Component({
  selector: 'app-images-container',
  templateUrl: './images-container.component.html',
  styleUrls: ['./images-container.component.scss']
})

/**
 * container for all image tiles
 * subscribes to GetImagesService in order to receive images filtered based on the current search query
 * @class ImagesContainerComponent
 */
export class ImagesContainerComponent implements OnInit, OnDestroy {

  /**
   * current page number
   * @property pageNumber
   * @type {number}
   */
  public pageNumber = 1;

  /**
   * array of pages to be displayed
   * @property page
   * @public
   */
  public page: IImagePage;

  /**
   * total amount of pages
   * @property pagesAmount
   * @public
   */
  public pagesAmount: number;

  /**
   * @property _subscriptions
   * @type {Subscription}
   */
  private _subscriptions: Subscription = new Subscription();

  /**
   * true if there is no search match
   * @property noMatch
   * @type {boolean}
   * @public
   */
  public noMatch = false;

  /**
   * @constructor
   * @param {GetImagesService} _getImagesService
   * @param {NgbModal} _modalService
   */
  constructor(private _getImagesService: GetImagesService, private _modalService: NgbModal) {
  }

  /**
   * angular on init lifecycle
   * @method ngOnInit
   * @public
   */
  public ngOnInit(): void {
    this.getCurrentPage();
    this.getPagesAmount();
  }

  /**
   * subscribes to GetImagesService currentPage behaviour subject
   * populates the view with the returned data
   * @method getCurrentPage
   */
  private getCurrentPage(): void {
    this._subscriptions.add(
      this._getImagesService.currentPage.subscribe((page: IImagePage) => {
        if (typeof page !== 'undefined' && page !== null) {
          this.noMatch = page.images.length > 0 ? false : true;
          this.page = page;
        }
      }));
  }

  /**
   * subscribes to filteredPagesAmount BehaviourSubject to retrieve total pages amount
   * @method getPagesAmount
   */
  private getPagesAmount(): void {
    this._subscriptions.add(this._getImagesService.filteredPagesAmount.subscribe((amount: number) => {
      this.pagesAmount = amount * 10; // used for collectionSize param of the paginator
      this.pageNumber = 1;
    }));
  }

  /**
   * page change handler
   * @method onPageChange
   * @public
   */
  public onPageChange(): void {
    this._getImagesService.changePage(this.pageNumber);
  }

  /**
   * open modal handler
   * @method openModal
   * @param {IImageData} imageData
   */
  public openModal(imageData: IImageData): void {
    const modalRef = this._modalService.open(ImageModalComponent, {centered: true});
    modalRef.componentInstance.imageData = imageData;
  }

  /**
   * angular on destroy lifecycle
   * @method ngOnDestroy
   * @public
   */
  public ngOnDestroy(): void {
    this._subscriptions.unsubscribe();
  }
}
